# agentctl

`agentctl` manages a single SSH agent on the system. It is intended to combine
the most commonly used commands to manipulate an SSH agent. The goal is to have 
a single instance of an SSH agent running for a user with a socket in a standard
location. This does not require any environment variables to be set to function
correctly. 

# Requirements

`agentctl` is written in Lua and requires a Lua interpreter and some Lua 
packages. 

## Interpreter

Lua >= 5.2

## Packages

These can be installed using [LuaRocks](https://luarocks.org). They may 
also be present in the system's repositories.

- [argparse](https://github.com/mpeterv/argparse)
- [luafilesystem](https://keplerproject.github.io/luafilesystem/)
- [luaposix](https://github.com/luaposix/luaposix)

# Installation

The `agentctl` script can be downloaded separately and copied to a directory 
that is in $PATH. 

# Usage

Run the following command on the terminal to see the list of commands and 
options available

```
agentctl -h
```
